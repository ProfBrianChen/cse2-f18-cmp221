//Cameron Pfeffer  CSE2  Lab6  10/11/18
//Pattern A
import java.util.Scanner; //imports scanner class for user input
public class PatternA{ //starts PatternA class
  public static void main(String [] args){ //main method for every program

    
    Scanner scnr = new Scanner(System.in); //Initialize scanner for use in user input
    int userCounter; //Initializes usercounter, the variable for how many numbers the user wants printed
    System.out.println("How many lines do you want? Enter an integer: "); //Prints a statement asking the user for input
    while (!scnr.hasNextInt()){ //Checks to make sure the user enters an integer
      System.out.println("Error, enter an integer: "); //Error message for when the user doesn't enter a correct input
      scnr.next(); //asks the user for a new input if their earlier input was invalid
    }
    userCounter = scnr.nextInt(); //sets the user counter equal to the integer the user input
    System.out.println(""); //spacing out for formatting
    System.out.println("");
    
    
      for(int row = 1; row<=userCounter; ++row){ //For loop, sets the row number to 1 to start out, checks that the row is less than the user counter, and increments the row at the end of the loop
        for(int n = 1; n<=row; ++n){ //n is the number to be printed. this loops prints and increments n until it is equal to the row
          System.out.print(n + " "); //prints n, this results in n being printed and incremented on the same line until it equals the row number
        } //loops the part that prints new n's
        System.out.printf("\n"); //Makes new line for the next row
      } //loops second loop
      
    } //end main method
      
  } //end class 
