//Cameron Pfeffer  CSE2  Lab6  10/11/18
//Pattern C
import java.util.Scanner;
public class PatternC{
  public static void main(String [] args){

    
    Scanner scnr = new Scanner(System.in);
    int userCounter;
    int loopCounter = 0;
    System.out.println("How many lines do you want? Enter an integer: ");
    while (!scnr.hasNextInt()){
      System.out.println("Error, enter an integer: ");
      scnr.next();
    }
    userCounter = scnr.nextInt();
    System.out.println("");
    System.out.println("");
    
    
      for(int row = 1; row<=userCounter; ++row){
        for(int spaces = 1; spaces<=(userCounter-row); spaces++ ){
            System.out.print(" ");
          }
        for(int n = 1; n<=row; ++n){
          
          System.out.print(row-(n-1));
        }
        System.out.printf("\n");
      }
      
    }
      
  }
