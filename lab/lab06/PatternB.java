//Cameron Pfeffer  CSE2  Lab6  10/11/18
//Pattern B
import java.util.Scanner;
public class PatternB{
  public static void main(String [] args){

    
    Scanner scnr = new Scanner(System.in);
    int userCounter;
    System.out.println("How many lines do you want? Enter an integer: ");
    while (!scnr.hasNextInt()){
      System.out.println("Error, enter an integer: ");
      scnr.next();
    }
    userCounter = scnr.nextInt();
    System.out.println("");
    System.out.println("");
    
    
      for(int row = 1; row<=userCounter; ++row){
        for(int n = userCounter; n>=row; --n){
          System.out.print(((userCounter-n)+1) + " ");
        }
        System.out.printf("\n");
      }
      
    }
      
  }
