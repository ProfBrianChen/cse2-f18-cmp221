// Cameron Pfeffer   CSE02  9/13/2018
// This program allows the user to input the total cost of a check and then splits it a user input number of ways
//

import java.util.Scanner; // imports Scanner Class

public class Check{ 
      
  public static void main(String[] args) { //main method for every program
    
    
    // taking inputs 
    
    Scanner myScanner = new Scanner( System.in); //declares new scanner looking for input from user
    
    System.out.print("Enter the original cost of the check in the form xx.xx "); //asks the user to input the cost of the check
    System.out.println(""); //spaces out the text for the next line
    double checkCost = myScanner.nextDouble(); //accepts input from user as a double named checkCost
    
    System.out.print("Enter the percent tip you wish to pay as a whole number in the form xx");
    System.out.println(""); //spaces out the text for the next line
    double tipPercent = ((myScanner.nextDouble())/100.0); //accepts next user input as the tip percent and divides it by 100
    
    
    System.out.print("Enter the number of people who went out to dinner");
    System.out.println(""); //spaces out the text for the next line
    int numPeople = (myScanner.nextInt());
    
    
    //Calculations
    
    double totalCost; //declares the variable of total cost as a double
    double costPerPerson; //declares the variable of cost per person as a double
    int dollars, dimes, pennies; //declares the variables of dollars, dimes, and pennies as integers
    
    totalCost = checkCost * (1 + tipPercent); //calculates cost of check with tip
    costPerPerson = totalCost / numPeople; //splits the check (tax included) between number of people 
    
    dollars = (int) costPerPerson; //calculates how many dollar bills will be needed
    dimes = (int) (costPerPerson * 10) % 10; //calculates number of dimes needed
    pennies = (int) (costPerPerson * 100) % 10; //calculates number of pennies needed
    
    //Output
    
    System.out.println ("Each person owes $" + dollars + "." + dimes + pennies); //prints amount owed by each person
                        
    
  } //end method
  
} //end class

  