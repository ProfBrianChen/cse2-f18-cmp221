// Cameron Pfeffer
// 9/4/18
// CSE2 WelcomeClass

public class WelcomeClass{
  
public static void main(String args[]){
  

  System.out.println("-----------");
  System.out.println("| WELCOME |");
  System.out.println("-----------");
  System.out.println(" ^  ^  ^  ^  ^  ^");
  System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println("<-C--M--P--2--2--1>");
  System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println(" v  v  v  v  v  v");
  System.out.println("I'm Cameron Pfeffer, I'm an Astrophysics Major from Chester County PA. I'm eating a yogurt parfait with granola and strawberry as I code this");
                      
}
}