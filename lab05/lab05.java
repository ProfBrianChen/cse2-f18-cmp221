// CSE 2  Cameron Pfeffer  10/4/18
// Reading bad inputs 

import java.util.Scanner; //import scanner

public class lab05{ //start class
  public static void main(String [] args){ //main method
      Scanner myScanner = new Scanner(System.in); //declare scanner for use in user input
    
    //Declaring variables 
    int courseNumber = 0; //I declare all the variables and give them dummy values so the compiler sees them in the outer scope of the program
    String depName = "";
    int meetings = 0;
    int timeHour = 0;
    int timeMin = 0;
    String instructorName = "";
    int studNumber = 0;
    
    
    
      // user input and checking
    
      System.out.println("Enter your course number: "); //asks user for course number 
      
    while (!myScanner.hasNextInt()){ //allows user input, checks if its an integer. If not, executes while loop
      System.out.println("Error, enter an integer for the course number: "); //Prints error, asks for a new int
      myScanner.next(); //overwrites scanner int, allows new input
       } //loops until an int is intered
      courseNumber = myScanner.nextInt(); //If while loop is exited, the value is an int so coursenumber is set to that int
      
      System.out.println("Enter the department name: "); //The rest of the program works the same way as what's listed above, asking for ints and strings were appropriate and giving error messages if the wrong type is entered
    while (!myScanner.hasNext()){
      System.out.println("Error, enter a string for the department name :");
      myScanner.next();
      }
      depName = myScanner.next();
 
    System.out.println("Enter how many times a week the class meets: ");
      while (!myScanner.hasNextInt()){
        System.out.println("Error, enter an integer for class meetings: ");
        myScanner.next();
      }
      meetings = myScanner.nextInt();
    
    System.out.println("Enter the hour the class meets (0-23)");
      while ((!myScanner.hasNextInt())){
        System.out.println("Error, enter an integer for the hour: ");
        myScanner.next();
      }
      timeHour = myScanner.nextInt();
    
    System.out.println("Enter the minute the class meets (00-59)");
    while ((!myScanner.hasNextInt())){
      System.out.println("Error, enter an integer for the minute: ");
      myScanner.next();
    }
   timeMin = myScanner.nextInt();
    
    System.out.println("Enter the instructor's last name");
    while ((!myScanner.hasNext())){
      System.out.println("Error, enter a string for the instructor's last name: ");
      myScanner.next();
    }
    instructorName = myScanner.next();
    
    System.out.println("Enter the number of students in the class: ");
    while((!myScanner.hasNextInt())){
      System.out.println("Error, enter an integer for the number of students in the class: ");
      myScanner.next();
    }
    studNumber = myScanner.nextInt();
    
    //Output
    
    System.out.println(""); //spacing out for formatting
    System.out.println("");
    System.out.println("");
    
    System.out.println("You have a class with these specifications: "); //Prints out a title to the data printed after
    System.out.println("");
    System.out.println("Department name and course Number:      " + depName + " " + courseNumber); //These next lines print all the data entered by the user
    System.out.println("                          Meeting:      " + meetings + "x per week");
    System.out.println("                               At:      " + timeHour + ":" + timeMin);
    System.out.println("                             With:      Professor " + instructorName + " and " + studNumber + " students");
    } //end main method
    
    
  }//end class 
