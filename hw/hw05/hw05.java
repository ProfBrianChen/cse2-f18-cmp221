//// Cameron Pfeffer   CSE2   10/9/2018
// This program will use my previous code from the card generator lab to generate a hand of cards and calculate the probability of certain hands
// by doing this a large amount of times specified by the user 
import java.util.Scanner; //imports scanner for use in user input
public class hw05{ //starts class of hw05
  public static void main(String [] args){ //start main method
    
    
  Scanner scnr = new Scanner(System.in);  //Declare scanner for user input
    int counter = 0; //declare counter to count the times the program loops
   
      System.out.println("How many hands do you want to draw? Enter an integer."); //Asks the user for input on how many simulations to run
    while (!scnr.hasNextInt()){ //loop to make sure the user enters an integer
      System.out.println("Error, enter an integer for how many hands you want to draw."); //Error message for if the user doesn't enter an integer
      scnr.next(); //looks for input from user after saying error message
    }
    int userCounter = scnr.nextInt(); //initialized scanner takes user input
    int cardInHand = 0; //variable to keep track of which card is being drawn
    int one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen; //This will be used to count the occurance of each card number in a single hand
    one = two = three = four = five = six = seven = eight = nine = ten = eleven = twelve = thirteen = 0; //dummy values to keep variables in scope
    int clubs, spades, hearts, diamonds; //this will be used to count the occurance of each suit in a single hand  
    clubs = spades = hearts = diamonds = 0; //dummy values to keep variables in scope
    int card0, card1, card2, card3, card4; //these variables record the number of the card drawn to insure it isn't repeated
    card0=card1=card2=card3=card4=0; //dummy values to keep variables in scope
    int cardNumber; //initial number drawn, 1-52
    cardNumber = 0;
    int twoCounter; //because two pair looks like 2 two of a kind, I use this variable to track if 2 two of a kind happen in one hand so I increment the right variable 
    int onePair, twoPair, threeOfAKind, fourOfAKind; //These variables are used to keep track of how many of each combination occur in the simulation
    onePair=twoPair=threeOfAKind=fourOfAKind=0; //dummy values to keep variables in scope
      
      
    while(counter<userCounter){ //Runs the code in the loop the amount of times the user input.
      
      cardInHand = 0; // resets hand counters 
      one = two = three = four = five = six = seven = eight = nine = ten = eleven = twelve = thirteen = clubs = spades = hearts = diamonds = 0; //resets card counters
      card0=card1=card2=card3=card4=0; //resets card identity holders  
        
    // I will now insert the code from lab04, the card generator. (slightly augmented to fit the needs of this program)
      
      
          while (cardInHand<5){ //When there are less than 5 cards in the hand it runs this code. This includes 0 actually, so it runs 5 times per simulation
                while((cardNumber == card0) || (cardNumber == card1) || (cardNumber == card2) || (cardNumber == card3) || (cardNumber == card4)){ //if any of the card numbers are equal to any of the previous card numbers in the hand, it executes this code. Because this includes, the card from the last time it ran (starting at the second time), it will always run again. It will loop until it generates a card that isn't in the hand. 
                  cardNumber = (int) ((Math.random()*52)+1); //Generates random number from 1 to 52 inclusive
                }
            
            
                  // This part of the program determines the card's suit 

                  String suit; //initializes suit variable
                  suit = ""; //I was getting bugs where the code didn't realize suit was initialized after my if statement, so I did this and it fixed it
                  int caseNumber = 0; //initializes the casenumber variable. I use this variable to guage what interval of numbers the generator made. I'll use it in the switch statement later. Switch statements can't take strings, so I couldn't use the suit itself.
                     if (cardNumber<=13){ //if the number generated is less than or equal to 13, it is diamonds 
                      suit = "Diamonds"; //Sets suit of card to diamond if it fits the above requirements 
                      caseNumber = 1; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
                  }
                     else if (cardNumber<=26){ //If the number is less than or equal to 26, it is clubs. This part only executes if the number is greater than 13
                       suit = "Clubs"; //Sets suit of card to clubs if it fits the above requirements 
                        caseNumber = 2; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
                  }
                     else if (cardNumber<=39){ //If the number is less than or equal to 39, it is hearts. This part only executes if the number is greater than 26
                       suit = "Hearts"; //Sets suit of card to hearts if it fits the above requirements 
                        caseNumber = 3; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
                  }
                     else if (cardNumber<=52){ //If the number is less than 52, it is Spades. This part only executes if the number is greater than 39
                       suit = "Spades"; //Sets suit of card to spades if it fits the above requirements 
                        caseNumber = 4; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
                  }

                   

                      //First, i'll use this part of the program to break down that 1-52 value into a 1-13 value
                  
                  int cardAssigner = 0; //the card assigner gives a 1-13 value to a number generated from the initial 1-52 number
                  switch(caseNumber){ //This switch uses the previously determined caseNumber from the earlier if statements to subtract the number down to a 1-13 number. 
                   case 1: 
                     cardAssigner = cardNumber; //in this case, the number generated was already from 1-13 so I left it alone
                     break;
                   case 2:
                      cardAssigner = cardNumber - 13; //in this case, the number was from 14-26, so i subtract 13 from it to get it from 1-13. 
                      break;
                   case 3:
                      cardAssigner = cardNumber - 26; //in this case the number was from 27-39, so i subtract 26 from it to get it from 1-13
                      break;
                   case 4:
                      cardAssigner = cardNumber - 39; //in this case the number was from 39-52, so i subtract 39 from it to get it from 1-13
                      break;

                 }
                  
                      switch(cardAssigner){ //This switch adds to the counter for each card pulled depending on which one got pulled.
                        case 1: //if card 1 (ace) got pulled, it adds to the counter of one cards pulled in the hand
                          ++one; //this increments the counter
                          break; //breaks out of the switch 
                        case 2:
                          ++two;
                            break;
                        case 3:
                          ++three;
                            break;
                        case 4:
                          ++four;
                            break;
                        case 5:
                          ++five;
                            break;
                        case 6:
                          ++six;
                            break;
                        case 7:
                          ++seven;
                          break;
                        case 8:
                          ++eight;
                          break;
                        case 9:
                          ++nine;
                          break;
                        case 10:
                          ++ten;
                          break;
                        case 11:
                          ++eleven;
                            break;
                        case 12:
                          ++twelve;
                          break;
                          }
            
            
                      switch(suit){ //I thought the program needed to take suits into account for flushes and stuff, but It didn't. Gonna leave this here anyway. 
                        case "Clubs":
                          ++clubs;
                          break;
                        case "Spades":
                          ++spades;
                          break;
                        case "Hearts":
                          ++hearts;
                          break;
                        case "Diamonds":
                          ++diamonds;
                          break;             
                 }
                       switch(cardInHand){ //here I assign a variable to the original number drawn. This is implemented in the random while loop to insure identical cards are not drawn to the same hand
                         case 0: //For the first card in the hand
                           card0=cardNumber; //for the first card, it stores the card's initial value in card0
                           break; //break from switch after hitting a case
                         case 1:
                           card1=cardNumber;
                           break;
                         case 2:
                           card2=cardNumber;
                           break;
                         case 3:
                           card3=cardNumber;
                           break;
                         case 4:
                           card4=cardNumber;
                             break;
                           
                       }
         ++cardInHand; //increments card in hand so it stops at 5 cards 
          }
      twoCounter = 0; //resets the two counter, which I use to see how many pairs of cards I have
      if(one==2){ //if there are two 1's, I execute this code
        ++twoCounter; //increments the two counter if there are two ones 
      }
       if(two==2){
        ++twoCounter;
      }
       if(three==2){
        ++twoCounter;
      }
       if(four==2){
        ++twoCounter;
      }
       if(five==2){
        ++twoCounter;
      }
       if(six==2){
        ++twoCounter;
      }
       if(seven==2){
        ++twoCounter;
      }
       if(eight==2){
        ++twoCounter;
      }
       if(nine==2){
        ++twoCounter;
      }
       if(ten==2){
        ++twoCounter;
      }
       if(eleven==2){
        ++twoCounter;
      }
       if(twelve==2){
        ++twoCounter;
      }
       if(thirteen==2){
        ++twoCounter;
      }
      
      switch (twoCounter){ //The outcome of the twoCounter can only be 0, 1, or 2. If it's 0, I don't want to do anything. But if it's 1, its a pair. If it's 2, its 2 pair. 
        case 1: //If there's one 2, I run this code
      ++onePair; //Adds to the counter keeping track of how many one pairs there are
        break; //breaks from switch
        case 2: //if there are two pairs, I run this code
      ++twoPair; //adds to the counter keeping track of how many two pairs there are
        break; //breaks from switch
      }
        if(one==3||two==3||three==3||four==3||five==3||six==3||seven==3||eight==3||nine==3||ten==3||eleven==3||twelve==3||thirteen==3){ //If any of the counters for how many of each number there are is equal to three, I run this code
          ++threeOfAKind; //Adds to the counter for 3 of a kind. 
        }
      else if(one==4||two==4||three==4||four==4||five==4||six==4||seven==4||eight==4||nine==4||ten==4||eleven==4||twelve==4||thirteen==4){ //If any of the counters for how many of each number there are is equal to four, I run this code
          ++fourOfAKind; //adds to the counter for 4 of a kind
        }
      
      ++counter; //adds to the counter keeping track of how many times a new hand has been generated
          }
  
    System.out.println("Program ran "+userCounter+" simulations");
    System.out.printf("\n The calculated probability of getting one pair is: %.3f%%    \n", 100.000*onePair/userCounter);
    System.out.printf("\n The calculated probability of getting two pairs is: %.3f%%    \n", 100.000*twoPair/userCounter);
    System.out.printf("\n The calculated probability of getting three of a kind is: %.3f%%  \n", 100.000*threeOfAKind/userCounter);
    System.out.printf("\n The calculated probability of getting four of a kind is: %.3f%% \n", 100.000*fourOfAKind/userCounter);
  }
}
