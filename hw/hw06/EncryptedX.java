// Cameron Pfeffer    CSE02   10/21/18
// Encrypted X program
import java.util.Scanner; //import scanner
public class EncryptedX{ //Start class
  public static void main(String [] args){ //main method for every program
    Scanner scnr = new Scanner(System.in); //declare scanner looking for user inputs 
    int lines;
    do{
    System.out.println("Enter an integer between 0 and 100 for the sides of the square"); //Prompts the user for an integer for the sides of the square 
    
    while((!scnr.hasNextInt())){ //Scanner looks for integer
      System.out.println("Error, enter an integer"); //prints error if scanner gets a non int value
      scnr.next(); //Looks for an int from user again 
    }
     lines = scnr.nextInt(); //Sets the number of lines to what the user asked for
      if(lines<0||lines>100){System.out.print("Error. ");} //Adds "error" to the printed message if there's an error. 
    }while(lines<0||lines>100); //loops the above code while the user's integer doesn't fall within range
    int n; //Initializes n, a placeholder to determine where a space should be in the square
    int x; //initialize x, a placeholder to determine where the program will print next
    
    for(int row = 1; row<=lines; ++row){ //loop sets the row number to start at 1, runs until the number of rows printed is equal to what the user asked for, and increments the row number every loop
      n=lines-row; //Gives a value to n. This is the place in the row where the second space will be. Every loop this gets smaller as the row number gets bigger
      for(x=1; x<=lines; x++){ //loop sets x to start at 1, checks runs until x is equal to the length of the side of the square, and increments x at the end of the loop
        if((x==row)||(x==(1+n))){ //Looks for where to put a space with an if statement. Because x holds the value of where the next character will be printed, it checks if x is one of the special numbers (n or the number of what row it is on) and prints a space if that is true
          System.out.print(" "); //prints a space
        }
        else{ //If x is not a special number, it prints a star
          System.out.print("*"); //prints a star
          
        }
      }
      System.out.print("\n"); //at the end of the first loop, a new line is made with this command. Then it loops
    }
    
    
    
    
  } //end main method
}//end class