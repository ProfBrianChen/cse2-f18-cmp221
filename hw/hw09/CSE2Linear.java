// Cameron Pfeffer   CSE2   CSE2Linear // 
import java.util.Scanner; //import scanner class
public class CSE2Linear{ //Starts class CSE2Linear
  public static void main(String [] args){ //main method for all programs
    int[] grades = new int[15]; //initializes the array to hold the 15 grades
    double grade = 0; //this value holds an individual grade before it gets put in the array
    int egrade = 0; //this value hold the last grade, to make sure the next grade is higher or equal
    Scanner scnr = new Scanner(System.in); //initializes scanner for use in user input
    
    System.out.println("Enter 15 integers between 0 and 100 inclusive in ascending order." ); //prompts user
    for(int i = 0; i<15; i++){ //for loop to get 15 grades
      grade = scnr.nextDouble(); //sets the grade variable to the user input
      
    if((grade<=100)&&(grade>=0)&&(grade==((int)grade))&&(grade >= egrade)){ //if the grade meets the criteria, it is put into the array with this if statement
      grades[i] = ((int)grade); //puts grade in array
      egrade = ((int)grade); //saves grade to egrade to make sure the next is >=
    }
    else{ //if a criteria is failed, it opens this else statement
      i--; //subtracts from the counter so it doesn't over run the array and cause a runtime error
      if(grade<0||grade>100){System.out.println("Error, enter a number between 0 and 100, inclusive.");} //if the grade is out of range, print this
      if(grade != ((int)grade)){System.out.println("Error, enter an integer for the grade.");} //if the grade is not an int, print this
      if(grade<egrade){System.out.println("Error, enter a grade greater than or equal to the one before it.");} //if the grade is less than the previous, print this
    }

       }
    System.out.println("\n The grades:"); //Prints a new line before the grades
    printer(grades); //calls the printer method to print the array of the grades
    
    System.out.print("Enter a grade to search for: "); //Prompts user to search for a grade
    int search = scnr.nextInt(); //scanner looks for grade to search for and calls it search
    binarySearch(grades, search); //uses the binary search method to look for the grade
    shuffle(grades); //shuffles the grades
    printer(grades); //prints shuffled grades
    System.out.println("Enter a grade to search for: "); //prompts user again
    search = scnr.nextInt(); //scanner looks for input for new grade to search for
    linearSearch(grades, search); //uses linear search to find the grade
    
  
  
  }
  
  public static void printer(int[] array){ //this method prints out an array
    System.out.print("\n");//new line for formatting
    for(int i = 0; i<15; i++){ //for loop to print all array fields
      System.out.print(array[i] + " "); //prints the ith component of the array, then gets incremented
    }
    System.out.print("\n"); //spacing for formatting
    return; // returns to main method
  }
  
  public static void binarySearch(int[] array, int grade){ //binary search uses low and high boundaries to calculate a middle value and check if it is what's being searched for
    int low = 0; //sets initial low to 0
    int high = 15;//and high to 15
    int mid = 7; // and the mid to 7
    int i = 0;//i is the number of iterations, starts at 0
    while((high>=low)&&(low<=high)){ //loops while the high and low aren't overlapping eachother
      i++;//increment high
      if(array[mid] == grade){ //if the grade is the middle value, it found it
        System.out.println("Grade was found in the list with " + i + " iterations. "); //prints that the grade was found
        return; //returns to main method
      }
      else if(array[mid] > grade){ //if the grade wasn't the middle value, and was less than it, this runs
        high -= (int)(14/(i+1)); //subtracts half of the current range from the high limit
      }
      else if(array[mid] < grade){ //if the grade was greater than mid this runs
        low += (int)(14/(i+1)); //adds half of the current range to the low limit
      }
        mid =((int)((high + low)/2)); //resets the middle to the new middle
    }
  System.out.println("Grade was not found with " +i + " iterations"); // if the grade isn't found, it prints that it was not found and how many iterations it took    
  
  }
  
 public static void shuffle(int[] grades){ //this method shuffles the cards
    int g1; //g1 is the random spot chosen for the first grade
    int g2; //g2 is the random spot chosen for the second grade
    for(int x = 0; x<50; x++){ //this for loops runs the shuffler 50 times
    for(int i = 0; i<15; i++){ //here, for every card, a random grade is chosen for it to switch with, this is done with a for loop
       g1=(int)(Math.random()*15); //the first grade is chosen randomly
       g2=(int)(Math.random()*15);// then the second
      while(g2==g1){//then i run a while loop to make sure the first grade isn't the same as the second one
        g2=(int)(Math.random()*15); //if it is the same, i randomize again
      }
      int g2n = grades[g2]; //then it saves the value of the card in the g2th slot 
      grades[g2] = grades[g1];//copies the value of the g1th slot into the g2th slot
      grades[g1]= g2n;//sets the g1th slot to the saved value of the g2th slot
    }
    }
    System.out.println(" \n Shuffled \n"); //prints that the shuffle has occured
  }
  
  
  public static void linearSearch(int [] grades, int search){ //linear search just checks each component of the array one by one
    int i = 0; //iterator 
    int n;// used to calculate iteratoins inside the loop because i iterates at the end
    for(i = 0; i<15; i++){ //for loop runs until i is 15
      if(grades[i] == search){ //checks if the grade at the ith component of the array is the one youre looking for, if it is, this runs
        n = i + 1; //adds one to n, because that's the iteration we're on here
        System.out.println("Grade was found with " + n + " iterations"); //prints that the grade was found and how many iterations it took
        return; //returns to main method
      }
    }
    System.out.println("Grade was not found with " + i + " iterations"); //if the whole loop runs and the grade was not found, it was not in the array.
  }
    
    
    
    
  }   
