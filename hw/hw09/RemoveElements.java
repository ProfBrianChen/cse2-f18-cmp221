// Cameron Pfeffer   CSE2   RemoveElements
//I will now input the given code: 
import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
    System.out.print("\n");
  	String out = "The original array is: \n";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  public static int[] randomInput(){ //this method creates an array of random numbers from 0-9
    int [] array = new int[10]; //initalizes a new array with 10 values in it
    for(int i = 0; i<10; i++){ //for loop to increment array component
      array[i] = (((int)(Math.random()*10))); //randomly generates a number from 0-9 puts it in the ith slot of the array
    }
    return array; //returns the random array
  }
  
  
  public static int[] delete(int [] list, int index){ //this method creates a new array identical to the input, minus one component that has been cut out
    int [] array = new int[list.length-1]; //creates a new array with one less component than the input
    Scanner scnr = new Scanner(System.in); //initializes scanner
    while(index>=list.length||index<0){ //while loop to check index is within bounds
      System.out.println("Error, index larger than list size or negative, enter an integer between 0 and " + (list.length-1) + ". "); //error message telling user to enter correct input
      index=scnr.nextInt(); //loops for input again
    }
    for(int i = 0; i<list.length; i++){ //for loop to remove component of array
      if(i<index){ //if the incrementor i is less than the index number, this runs
        array[i] = list[i]; //sets the ith component of the new array to the ith component of the old array
      }
      else if(i>index){ //once the incrementor is greater than the index, the component of the new array being assigned is going to be one less than i, because we've skipped over a number. So this different code runs
        array[i-1] = list[i]; //sets the (i-1)th component of the new array to the ith component of the old array
      }
    }
    return array; //returns new array
  }
  
  public static int [] remove(int [] list, int target){ //this method returns an array that is missing every component where the value of the array at that component is a target number
    int n = 0; //I use this n variable to figure out how long the new array will be with the undesired components cut out
    for(int i=0; i<list.length;i++){ //this for loop increments n whenever the value of the ith component of the old array is the same as the target value
      if(list[i]==target){//read above
        n++; //increments n. once the loop is over, n will be the number of instances of the number we want to remove
      }
    }
    
    if(n==0){return list;} //if the number is not in the list, just returns an identical list.
    
    int [] array = new int[list.length - n]; //creates new array. It's length will be the length of the old one minus the number of instances of the target number
    int k = 0; //I use the variable k to count how many times the target number has been encountered in forming the new array, this way I can subtract k from i to get the component of the new array we're going to fill
    for(int j = 0; j<list.length; j++){ //for loop to fill new array
      if(list[j] != target){ //when the value of the jth component of the old array isn't equal to the target, this runs
        array[j-k] = list[j]; //sets the value of the component of the new array (adjusted for already encountered target numbers), to the ith value of the old array
      }
      else if(list[j] == target){ //if the jth value of the list is the target number, it increments k, which keeps track of how many times the target has been encountered
        k++; //increments k
      }
    }
    return array; //returns the new array
  }
  
  
  
  
}
