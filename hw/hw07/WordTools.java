import java.util.Scanner; //Import scanner for use in user input
public class WordTools{ //start class
  public static void main(String [] args){ //start main method
    Scanner scnr = new Scanner(System.in); //initialize scanner for use in user input
    String again = " "; //initalizes the again string which I'll use to decide whether the user wants to input text again
    do{ //do loop for text input part
    String sample = sampleText(); //initalizes the sample text by calling the sample text method
      again = "new";
          System.out.println(""); //spacing out for formatting 
          System.out.println("");
          System.out.println("");
    System.out.println("You entered: " + sample+"\n"); //Displays what the user entered 
    printMenu(sample); //calls the printmenu method, which brings up the menu
    System.out.println("Would you like to quit the program or enter a new text? (quit/new)"); //After the menu has been exited, this part asks if the user wants to also exit the program, or input new text
    again = scnr.next(); //asks user for input on new text
    }while(!again.equals("quit")); //loops this while the user has not entered the word quit for again
    return; //Ends program when the loop is exited
      
  }
  public static void printMenu(String sample){ //menu method, which will display a menu and call methods based on what the user enters
    String option; //this string is the thing the user enters for the menu option
    int i; //i is used later to count the number of non whitespace characters. I keep variables a that are the same through methods
    int w; //w is used to count the number of words in a sample text 
    String text; //this string is the text the user enters for the find text feature
    do{ //This loop makes the menu keep popping up after a method is exited unless the user enters the quit method, which backs out of the menu method entirely 
    Scanner scnr = new Scanner(System.in); //scanner for user input
    System.out.println("Enter which option you want"); //self explanatory, these are the choices for the user
    System.out.println("c   --   Number of non-whitespace characters");
    System.out.println("w   --   Number of words");
    System.out.println("f   --   Find text");
    System.out.println("r   --   Replace all !'s");
    System.out.println("s   --   Shorten spaces");
    System.out.println("q   --   Quit menu");
    option = scnr.next();
    while ((!option.equals("c"))&&(!option.equals("w"))&&(!option.equals("f"))&&(!option.equals("r"))&&(!option.equals("s"))&&(!option.equals("q"))){
    System.out.println("Error. Enter which option you want"); //the above statement prints an error if the user enters a string that's not an option.
    System.out.println("c   --   Number of non-whitespace characters");
    System.out.println("w   --   Number of words");
    System.out.println("f   --   Find text");
    System.out.println("r   --   Replace all !'s");
    System.out.println("s   --   Shorten spaces");
    System.out.println("q   --   Quit menu");
      scnr.next();
    }
    switch(option){ //this switch is used to run the methods determined by the users choice of menu option
      case "q": //in this case, the user wants to quit, so it just quits the method back to the main method. 
        return; //quits method
      case "c": //In this case, the user wants the number of non whitespace characters, this switch option calls that method
        i = getNumOfNonWSCharacters(sample); //calls method that gets number of non whitespace characters
        System.out.println("\n There are " +i+" non whitespace characters. \n"); //prints the number the user requested 
        break; //breaks switch back to menu loop
      case "w": //this option calls the method for getting the number of words
        w = getNumOfWords(sample); //calls said method
        System.out.println("\n There are " +w+" Words. \n"); //prints number of words
        break; //breaks switch back to menu loop
      case "f": //this option calls executes code to find a user entered text
        System.out.println("\n Enter the text you want to find"); //asks user for said text
        text = scnr.next(); //scanner looks for input
        System.out.println("There are " + findText(sample, text) + " instances of '" + text + "' in the sample text. \n"); //prints out how many times that text occurs in user sample by calling the find text method
        break; //break switch back to menu loop
      case "r": //runs code to replace ! with . in the text
        System.out.println("\n Edited Text: \n"); //displays edited text
        System.out.println(replaceExclamation(sample) + "\n"); //runs method to edit text
        System.out.println("Would you like to set the save the edited text? (y/n)"); //asks the user if they want to save the new text or keep the old text
        if((scnr.next()).equals("y")){sample = replaceExclamation(sample);} //if the user wants to save the text, the code updates the "sample" variable to be the new text
        break;//breaks switch back to menu loop
      case "s":// this case edits the text to turn double spaces into single spaces 
        System.out.println("\n Edited Text: \n"); 
        System.out.println(shortenSpace(sample) + "\n"); //runs code to edit text, prints it
        System.out.println("Would you like to set the save the edited text? (y/n)"); //asks user if they want to save the edited text
        if((scnr.next()).equals("y")){sample = shortenSpace(sample);} //if user wants to save edited text, updates sample variable to be the new text
          
        break; //break switch back to menu
          
    }
     
     
    
    }while(true); //continuously runs the menu after switch is broken. Menu can only be exited by exiting the method through the q option
  }
  
  
  public static String sampleText(){ //the sample text method allows user input of a string to edit
    Scanner scnr = new Scanner(System.in); //initalizes scanner for use in user input
    System.out.println("Enter a sample text"); //asks user for text
    String sample = scnr.nextLine(); //sets sample variable equal to what the user entered
    return sample; //returns sample to main method
  }
  
  
  public static int getNumOfNonWSCharacters(String sample){ //method to get the number of non whitespace characters
    int i = 0; //i is used to count non whitespace characters
   for(int x = 0; x<sample.length(); x++){ //loops while x, our incrementor, is less than the length of the sample
     if((!(sample.charAt(x) == ' '))&&(!(sample.charAt(x) == (char)'\n'))&&(!(sample.charAt(x) == (char)'\t'))&&(!(sample.charAt(x) == (char)'\r'))){
       ++i; //increments i if the character at the incrementors spot is not a space, new line, tab, or \r
     }
      
   }
    return i; //returns number of non whitespace characters
    
  }
  
  
  public static int getNumOfWords(String sample){ //This method returns the number of words
     int w = 0; //w is the number of words
     int x= 0; //x is our incrementor
   for( x = 0; x<sample.length(); x++){ //loops same as the last method
     if(((sample.charAt(x) == ' '))||((sample.charAt(x) == (char)'\n'))||((sample.charAt(x) == (char)'\t'))||((sample.charAt(x) == (char)'\r'))){
       ++w;//increments w if a whitespace is hit
     }
     else if(((x==(sample.length()-1)))&&(!(sample.charAt(x) == ' '))){
       ++w; //increments w if the last character is a space
     }
     if(((sample.charAt(x) == ' ')&&((sample.charAt(x-1) == ' ')))){
       w-=1; //subtracts from w if w is added to twice due to a double space
     }
     
   }
    return w; //returns word count to main method
  }
  
  public static String shortenSpace(String sample){ //method used to shorten spaces
    return(sample.replaceAll("  ", " ")); //replaces all double spaces in sample with single spaces and returns the sample
   
    
  }
  
  public static String replaceExclamation(String sample){ //method turns ! into .
    return(sample.replace('!','.')); //replaces all ! with . in sample, returns new string to main method
  }
  
public static int findText(String sample, String text){ //method counts the number of times a user entered string occurs in the sample
  int matches = 0; //sets initial matches in strings to 0
  for(int x = 0; x<(sample.length());x++){ //loop increments x until it is the same as the sample length
    if(sample.regionMatches(x,text,0,text.length())){ //starts at the xth character in sample, searches for the the user entered text in the sample and returns boolean
      ++matches; //if the boolean is true, increments matches
    }
  }
  return matches; //returns number of matches
}
  
  
  
  
  
  
  
}