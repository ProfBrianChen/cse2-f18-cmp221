//Cameron Pfeffer   11/12/18   Homework 8 

//Copying base code from homework file over 
import java.util.Scanner;
import java.lang.Math;
public class hw08{ 
public static void main(String [] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
} 

System.out.println("\n These are the cards: \n ");
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
  if(index==1){ //I added this part to the copied program. It checks if index is 1 (the last number index will be before it goes negative)
    index = 51; //Then it resets the index
    System.out.println("\n Out of cards, shuffling"); //says it's shuffling the cards
    shuffle(cards); //and calls the shuffle method to shuffle them
    printArray(cards); //it then prints the new cards
  }
   System.out.println("\n Enter a 1 if you want another hand drawn \n"); 
   again = scan.nextInt(); 
}  
  }
  public static void printArray(String[] cards){ //this method prints the array
  for(int i = 0; i<(cards.length); i++){ //It uses a for loop to increment a variable until it equals the length of the array it's printing
    System.out.print(cards[i]+" "); //for every increment of i, it prints out the value in the ith slot of the array followed by a space
  }
    System.out.println("\n"); //when the loop is over, it prints a space line
  return; //then ends the method
}
  public static void shuffle(String[] cards){ //this method shuffles the cards
    int c1; //c1 is the random spot chosen for the first card
    int c2; //c2 is the random spot chosen for the second card
    for(int x = 0; x<50; x++){ //this for loops runs the shuffler 50 times
    for(int i = 0; i<52; i++){ //here, for every card, a random card is chosen for it to switch with, this is done with a for loop
       c1=(int)(Math.random()*52); //the first card is chosen randomly
       c2=(int)(Math.random()*52);// then the second
      while(c2==c1){//then i run a while loop to make sure the first card isn't the same as the second one
        c2=(int)(Math.random()*52); //if it is the same, i randomize again
      }
      String c2n = cards[c2]; //then it saves the value of the card in the c2th slot 
      cards[c2] = cards[c1];//copies the value of the c1th slot into the c2th slot
      cards[c1]= c2n;//sets the c1th slot to the saved value of the c2th slot
      
        
    }
    }
    System.out.println(" \n Shuffled \n"); //prints that the shuffle has occured
  }
  
  public static String[] getHand(String[] cards, int index, int numCards){ //this method generates hands from the top cards on the shuffled deck
    String [] hand = new String[5]; //creates the hand array which stores the generated hand
    int stop = index - numCards; //uses the int variable stop to determine when to stop generating cards for the hand, it's essent
     int i = 0; //sets i to 0. I use i as a sort of second incrementor for the for loop. It only runs once per method call so it'll get set back to 0 if a new hand is needed
    for( ; index!=stop; --index){ //this for loop runs while the index isn't at the stop number yet, and subtracts from the index every loop
     
      hand[i]=cards[cards.length - index -1]; //sets the ith position in the new hand to the part of the card deck that I want it to. It's complicated but it works
      ++i; //increments i
      
    }
    System.out.println("Here's your hand: ");//prints "heres your hand" 
    return hand; //returns the hand to the main method. 
  }
}

