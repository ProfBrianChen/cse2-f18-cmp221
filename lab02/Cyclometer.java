// Cameron Pfeffer    9/6/18    CSE2
// Cyclometer
// This program Prints the number of minutes, number of counts, and distance per trip as well as the distance of the last two trips combined
//
public class Cyclometer { //main method required for Java
    public static void main (String[] args) {
      //input our data for trip time and counts
      int secsTrip1=480; //seconds in trip 1
      int secsTrip2=3220; //seconds in trip 2
      int countsTrip1=1561; //counts in trip 1
      int countsTrip2=9037; //counts in trip 2
      //input our data for wheel diameter, constants, conversions, etc
      double wheelDiameter=27.0; //diameter of wheel, will be used with counts to determine distance
      double PI=3.14159; //value for pi
      int feetPerMile=5280; //how many feet in a mile, to be used in conversion
      int inchesPerFoot=12; //how many inches in a foot, to be used in conversion
      int secondsPerMinute=60; //how many seconds in a minute, also to be used in conversion
      double distanceTrip1, distanceTrip2, totalDistance; //specifies what kind of variable these values will be. They will be calculated later
       //now the code will print the infromation for trip 1
          System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts."); //prints length of trip 1 in minutes and how many counts in trip 1
          System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts."); //prints length of trip 2 in minutes and how many counts in trip 2
      //Now the code will calculate the distance in both trips as well as the total distance
      distanceTrip1=((countsTrip1*wheelDiameter*PI)/(inchesPerFoot*feetPerMile)); //multiplies trip 1's counts by the diameter of the wheel and Pi to get the distance in trip 1, converts to miles
      distanceTrip2=((countsTrip2*wheelDiameter*PI)/(inchesPerFoot*feetPerMile)); //multiplies trip 2's counts by the diameter of the wheel and Pi to get the distance in trip 2, converts to miles
      totalDistance=distanceTrip1+distanceTrip2; //adds the two distances together to get the total
      //Now the code will print the distance information
          System.out.println("Trip 1 was "+distanceTrip1+" miles.");
          System.out.println("Trip 2 was "+distanceTrip2+" miles.");
          System.out.println("The total distance was "+totalDistance+" miles.");
     } // end of main method
} // end of class