// Cameron Pfeffer
// 9/4/18
// CSE2 WelcomeClass

public class Arithmetic{ 

public static void main(String args[]){
  
  // inputing variables
  
    int numPants=3; //number of pants
    double pricePants=34.98; //price of pants
  
    int numShirts=2; //number of shirts
    double priceShirts=24.99; //price of shirts
  
    int numBelts=1; //number of belts
    double priceBelts=33.99; //price of belts
  
    double salesTax=.06; //tax rate
  
  //Calculations
    
    double totalCostPants=numPants*pricePants; //calculates price of pants before tax
    double taxPants = (int)((salesTax * totalCostPants) * 100) /100.00; //calculates tax on pants and truncates it at the 100ths place
      
      
     double totalCostShirts=numShirts*priceShirts; //calculates price of shirts before tax
     double taxShirts = (int)((salesTax * totalCostShirts) * 100) / 100.00; //calculates tax on shirts and truncates it at the 100ths place
    
     double totalCostBelts=priceBelts; //calculates price of belts before tax
     double taxBelts = (int)((salesTax * totalCostBelts) * 100) / 100.00; //calculates tax on belts and truncates it at the 100ths place
    
     double taxTotal = taxPants + taxShirts + taxBelts;
    
  //Printing the information
  System.out.println("The total cost of pants before tax is $"+totalCostPants); //prints total cost of pants before tax
  System.out.println("The total cost of shirts before tax is $"+totalCostShirts); //prints total cost of shirts before tax
  System.out.println("The total cost of belts before tax is $"+totalCostBelts); //prints total cost of belts before tax
  System.out.println("The total cost of all items before tax is $"+(totalCostPants+totalCostShirts+totalCostBelts)); //prints total cost of all items before tax added together 
  System.out.println("The total tax on pants is $"+taxPants); //prints tax on pants
  System.out.println("The total tax on shirts is $"+taxShirts); //prints tax on shirts
  System.out.println("The total tax on belts is $"+taxBelts); //prints tax on belts
  System.out.println("The total tax on all items is $"+(taxTotal)); //prints total tax on all items added together
  System.out.println("The total cost of all items after tax is $"+(totalCostPants+totalCostShirts+totalCostBelts+taxTotal)); //prints total cost of all items after tax by adding all before tax values to all tax values
  }
}