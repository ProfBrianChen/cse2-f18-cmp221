// Cameron Pfeffer   CSE02   9/20/18
// Card Generator 
// This program generates a random card out of a deck of 52 
public class CardGenerator{ //start cardgenerator class
 
  public static void main(String [] args) { //main method for every program
    
    int cardNumber = (int) ((Math.random()*52)+1); //Generates random number from 1 to 52 inclusive
   
    // This part of the program determines the card's suit 
    
    String suit; //initializes suit variable
    suit = ""; //I was getting bugs where the code didn't realize suit was initialized after my if statement, so I did this and it fixed it
    int caseNumber = 0; //initializes the casenumber variable. I use this variable to guage what interval of numbers the generator made. I'll use it in the switch statement later. Switch statements can't take strings, so I couldn't use the suit itself.
       if (cardNumber<=13){ //if the number generated is less than or equal to 13, it is diamonds 
        suit = "Diamonds"; //Sets suit of card to diamond if it fits the above requirements 
        caseNumber = 1; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
    }
       else if (cardNumber<=26){ //If the number is less than or equal to 26, it is clubs. This part only executes if the number is greater than 13
         suit = "Clubs"; //Sets suit of card to clubs if it fits the above requirements 
          caseNumber = 2; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
    }
       else if (cardNumber<=39){ //If the number is less than or equal to 39, it is hearts. This part only executes if the number is greater than 26
         suit = "Hearts"; //Sets suit of card to hearts if it fits the above requirements 
          caseNumber = 3; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
    }
       else if (cardNumber<=52){ //If the number is less than 52, it is Spades. This part only executes if the number is greater than 39
         suit = "Spades"; //Sets suit of card to spades if it fits the above requirements 
          caseNumber = 4; //I use this variable in this if statement to catagorize the number so I know what to subtract from it later to make random number between 1 and 13 out of the initial random number 
    }
    
    // This part of the program determines the card's name 
    
        //First, i'll use this part of the program to break down that 1-52 value into a 1-13 value
    String cardName; //initializes variable cardName
    int cardAssigner = 0; //the card assigner gives a 1-13 value to a number generated from the initial 1-52 number
    switch(caseNumber){ //This switch uses the previously determined caseNumber from the earlier if statements to subtract the number down to a 1-13 number. 
     case 1: 
       cardAssigner = cardNumber; //in this case, the number generated was already from 1-13 so I left it alone
       break;
     case 2:
        cardAssigner = cardNumber - 13; //in this case, the number was from 14-26, so i subtract 13 from it to get it from 1-13. 
        break;
     case 3:
        cardAssigner = cardNumber - 26; //in this case the number was from 27-39, so i subtract 26 from it to get it from 1-13
        break;
     case 4:
        cardAssigner = cardNumber - 39; //in this case the number was from 39-52, so i subtract 39 from it to get it from 1-13
        break;
     
   }
    cardName = "a " + cardAssigner; // Here I set the card name to a string that is just the int that was stored in cardAssigner.   
  switch(cardAssigner){ //if the card assigner number was 1 (which should be ace), 11(which should be jack)...etc, then it triggers a case in this switch that changes the name to what it should be
    case 1: 
      cardName = "an Ace"; //if the card assigner number was 1, it makes it an ace, includes part of the sentence in which it will be printed so it says "an ace" and not "a ace" 
         break;
    case 11:
      cardName = "a Jack"; //if the card assigner number was 11, it makes it a jack
        break;
    case 12:
      cardName = "a Queen"; // if the card assigner number was 12, it makes it a queen
        break;
    case 13:
      cardName = "a King"; //if the card assigner number was a 13, it makes it a king
        break;
      //if none of the switch cases were triggered, it leave the name as its card assigner number (2-10)
      //now all the cards are named what they should be, so we can print the output. 
  }
 System.out.println("You drew " +cardName + " of " + suit); //prints the name and suit of the card
    
  }
}