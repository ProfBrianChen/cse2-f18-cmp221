// Cameron Pfeffer   CSE02   9/17/18
// Pyramid Program
// This program prompts the user for inputs of the size of a square side of a pyramid and the height
// of a pyramid, it then calculates the volume 

import java.util.Scanner; //imports scanner class for use in acquiring user input

public class Pyramid{ //Start Pyramid Class
  
  public static void main(String[]args){ //main method
    
    //the program will now prompt the user for input and acquire data
    Scanner myScanner = new Scanner(System.in); //Declares new scanner to get user input
    
        //Sides input
    System.out.print("Input the length of one side of the square pyramid in inches: "); //prompts the user for input a side in inches
    System.out.println(); //spaces out for input on the next line
    double pyramidSides = myScanner.nextDouble(); // Looks for user input on sides
        
        //Height input
    System.out.print("Input the height of the square pyramid in inches: ");
    System.out.println(); //spaces out for input on the next line
    double pyramidHeight = myScanner.nextDouble(); // looks for user input on the height
    
    
    //Calculation 
    
    double squareSides = Math.pow(pyramidSides, 2);
    double Volume = ((squareSides * pyramidHeight)/3.00);
    
    // output
    System.out.println("The volume of the pyramid is " + Volume + " cubic inches");
    
    
  } //end main method
  
  
} //end class