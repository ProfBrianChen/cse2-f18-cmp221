// Cameron Pfeffer    CSE02   9/16/18
//Convert program
//This program will take user input for the area affected by a hurricane and total rainfall. It will then convert it to cubic miles of rain over the area.
//

import java.util.Scanner; //imports scanner

public class Convert{ //start convert class 
  
  public static void main(String[] args) { //main method
    
    //taking inputs 
    
    Scanner myScanner = new Scanner(System.in); //declares new scanner looking for input from the user
    
      System.out.print("Input the affected area in acres: "); //asks for input of area
      System.out.println(); //creates next line for user input
      double Acres = myScanner.nextDouble(); //looks for a double from the user and calls it the variable "Acres" 
      
      System.out.print("Input the rainfall in the area in inches: "); //asks for input of rainfall
      System.out.println(); //creates next line for user input
      double Inches = myScanner.nextDouble(); //looks for a double input from the user and calls it "Inches"
    //converting to miles and calculating 
    
      double areaMiles = Acres/640.0; //converts acres to square miles (640 acres in a mile)
      double rainMiles = Inches/63360.0; //converts inches of rain to miles of rain
    
      double rainVolume = areaMiles*rainMiles;
    
    //output
    
    System.out.println("The volume of rain is " + rainVolume + " cubic miles.");
      
  } //end main method 
} //end class
  