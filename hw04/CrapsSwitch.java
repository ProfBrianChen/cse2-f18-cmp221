// Cameron Pfeffer   CSE02  9/24/18
// Craps Switch
//This program will generate or allow user input of dice rolls and name the outcome with Craps slang

import java.util.Scanner; //imports scanner
public class CrapsSwitch{ //starts Craps Switch class
  public static void main(String [] args) { //main method for every program
    
    Scanner myScanner = new Scanner(System.in); //Declares scanner
    Scanner myScanner2 = new Scanner(System.in); // Declares second scanner, to be used when two dice are input
    
    
    System.out.println("Would you like to input your dice? If you would, input '1'. If you would like to randomize your dice, put '2'. "); //Asks if user would like to input dice or not
    int ynInput; //initializes the input for yes and no
    ynInput = myScanner.nextInt(); //Initializes a string to be used to interpret user answer
  
    
    int die1 = 0; //initializes die 1 and gives it a dummy value
    int die2 = 0; //initializes die 2 and gives it a dummy value
    
    
    switch(ynInput){ //This switch determines whether the user wants to get random dice or input their own
      case 1: //In this case, the user wants to input their dice
          System.out.println("Input the value for die 1 now: "); //asks user for value for die 1
          die1 = myScanner.nextInt(); //takes input for die 1
          System.out.println("Input the value for die 2 now: ");  //asks user for value for die 2
          die2 = myScanner2.nextInt(); //takes input for die 2
        break; //ends case
      case 2: //In this case, user wants random dice
          die1 = (int) (Math.random()*6 +1); //randomizes die 1
          die2 = (int) (Math.random()*6 +1); //randomizes die 2
        break; //Ends case
      default: //If the user does not input 1 for yes or 2 for no gives an error
         System.out.println("Error, enter a 1 or a 2"); //printed error message
    }
    
    System.out.println(""); //spacing out to format output better
    System.out.println("");
    // I'm only going to comment on one of these switch statements, they're all the same thing repeated for different cases
    switch(die1){ //This switch determines what to do with the number rolled for die 1
      case 1: //In this case, Die 1 has a value of 1
          System.out.println("You rolled a " + die1 + " and a " + die2); //Prints out what dice were rolled
          switch(die2){ //this switch determines what to do with the number rolled for die 2, given that die 1 was already determined to have a value of 1
            case 1: //In this case, die 2 has a value of one
                System.out.println("Snake Eyes!"); //Die 1 and Die 2 both having values of 1 is called Snake Eyes. This is printed. 
                break; //Ends case of Die 2 having a value of 1
            case 2: //In this case, die 2 has a value of 2
                System.out.println("Ace Deuce!"); //One die having a value of 1 and the other having a value of two is called Ace Deuce. This is printed here
                break; //Ends case of Die 2 having a value of 2
            case 3: ///////////////////////////////////////////////And so on for the rest of the cases. This pattern repeats with different names for each combination of values
                System.out.println("Easy Three!");
                break;
            case 4:
                System.out.println("Fever Five!");
                break;
            case 5:
                System.out.println("Easy Six!");
                break;
            case 6:
                System.out.println("Seven Out!");
                break;
            default: //If none of the above conditions are met, it is considered a user error
              System.out.println("Error, enter a number from 1-6 for die 2"); //prints an error message if the user input an invalid value for die 2
            
          }//Ends switch for die 2
         break;//Ends case of Die 1 having a value of 1 
        
      case 2: //Starts case of die 1 having a value of 2, repeats above with different names for different combinations 
        System.out.println("You rolled a " + die1 + " and a " + die2);
          switch(die2){
            case 1:
                System.out.println("Ace Deuce!");
                break;
            case 2:
                System.out.println("Hard Four!");
                break;
            case 3:
                System.out.println("Fever Five!");
                break;
            case 4:
                System.out.println("Easy Six!");
                break;
            case 5:
                System.out.println("Seven Out!");
                break;
            case 6:
                System.out.println("Easy Eight!");
                break;
            default:
              System.out.println("Error, enter a number from 1-6 for die 2");
               
          }
         break;
        
      case 3:
            System.out.println("You rolled a " + die1 + " and a " + die2);
          switch(die2){
            case 1:
                System.out.println("Easy Four!");
                break;
            case 2:
                System.out.println("Fever Five!");
                break;
            case 3:
                System.out.println("Hard Six!");
                break;
            case 4:
                System.out.println("Seven Out!");
                break;
            case 5:
                System.out.println("Easy Eight!");
                break;
            case 6:
                System.out.println("Nine!");
                break;
            default:
              System.out.println("Error, enter a number from 1-6 for die 2");
               
          }
         break;
        
      case 4:
            System.out.println("You rolled a " + die1 + " and a " + die2);
          switch(die2){
            case 1:
                System.out.println("Fever Five!");
                break;
            case 2:
                System.out.println("Easy Six!");
                break;
            case 3:
                System.out.println("Seven Out!");
                break;
            case 4:
                System.out.println("Hard Eight!");
                break;
            case 5:
                System.out.println("Nine!");
                break;
            case 6:
                System.out.println("Easy Ten!");
                break;
            default:
              System.out.println("Error, enter a number from 1-6 for die 2");
              
          }
         break;
        
      case 5:
            System.out.println("You rolled a " + die1 + " and a " + die2);
          switch(die2){
            case 1:
                System.out.println("Easy Six!");
                break;
            case 2:
                System.out.println("Seven Out!");
                break;
            case 3:
                System.out.println("Easy Eight!");
                break;
            case 4:
                System.out.println("Nine!");
                break;
            case 5:
                System.out.println("Hard Ten!");
                break;
            case 6:
                System.out.println("Yo-leven!");
                break;
            default:
              System.out.println("Error, enter a number from 1-6 for die 2");
               
          }
         break;
        
      case 6:
            System.out.println("You rolled a " + die1 + " and a " + die2);
          switch(die2){
            case 1:
                System.out.println("Seven Out!");
                break;
            case 2:
                System.out.println("Easy Eight!");
                break;
            case 3:
                System.out.println("Nine!");
                break;
            case 4:
                System.out.println("Easy Ten!");
                break;
            case 5:
                System.out.println("Yo-leven!");
                break;
            case 6:
                System.out.println("Boxcars!");
                break;
            default:
              System.out.println("Error, enter a number from 1-6 for die 2");
           
          }
         break;
        
      default: //If none of the above conditions are met for die 1, it is assumed to be user error
        System.out.println("Error, enter a number from 1-6 for die 1"); //Error message telling the user to input correct values for die 1
        
          
        
    } //End switch for die 1
    
   System.out.println(""); //Spacing out for formatting. Makes it more readable 
   System.out.println(""); 
    
  }//End main method
  
}//End class
    
