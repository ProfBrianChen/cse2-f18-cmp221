// Cameron Pfeffer   CSE02  9/24/18
// Craps If
//This program will generate or allow user input of dice rolls and name the outcome with Craps slang


import java.util.Scanner; //imports scanner
public class CrapsIf{ //starts Craps If class
  public static void main(String [] args) { //main method for every program
    
  
    
    Scanner myScanner = new Scanner(System.in); //Declares scanner
    Scanner myScanner2 = new Scanner(System.in); // Declares second scanner, to be used when two dice are input
    
    
    System.out.println("Would you like to input your dice? If you would, input 'Yes'. If you would like to randomize your dice, put 'No'. "); //Asks if user would like to input dice or not
    String ynInput; 
    ynInput = myScanner.nextLine(); //Initializes a string to be used to interpret user answer
  
    
    int die1 = 0; //initializes die 1 and gives it a dummy value
    int die2 = 0; //initializes die 2 and gives it a dummy value
    
    if (ynInput.equals("Yes")) { //This part dictates what happens if the user wants to input dice
      System.out.println("Enter the first die's number: "); //asks for number of first die
      die1 = myScanner.nextInt(); //allows input for die 1
      System.out.println("Enter the second die's numeber: "); //asks for number of second die
      die2 = myScanner2.nextInt();  // allows input for die 2
    }
    
    else if (ynInput.equals("No")){ //this part dictates what happens if the user wants random dice
      die1 = (int) (Math.random()*6 +1); //randomizes die 1
      die2 = (int) (Math.random()*6 +1); //randomizes die 2
    }
    else { //If above conditions are not met, assumes user misspelled Yes or No resulting in an error
      System.out.println("Error, enter Yes or No. Case sensitive"); //Error message instructing user how to avoid error
      return; //Ends program due to error
    }
    
    
    System.out.println(""); //spacing out for formating of output
    System.out.println("");
    
    
     if ((die1 > 6)|(die1 < 1)|(die2 > 6)|(die2 < 1)) { //Error perameters for die 1 and 2. Basically says if die 1 or die 2 have values greater than 6 or less than 1, print error message
      System.out.println("Error, enter a number from 1 to 6"); //Returns error message if die 1 or 2 has bad input
       return; //ends program due to error
    }
    
    
    System.out.println("You rolled a " + die1 + " and a " + die2); //Prints the dice you rolled before giving them a name
    
    
    if (die1 == 1){ //Determines what happens if die 1 has a value of 1
      if (die2 == 1){System.out.println("Snake Eyes!");} //Given die 1 has a value of 1 already, a value of 1 for die 1 translates to snake eyes
      else if (die2 == 2){System.out.println("Ace Deuce!");}// given die 1 has a value of 1 already, a value of 2 for die 2 translates to an Ace Deuce.
      else if (die2 == 3){System.out.println("Easy Four!");}//This continues for different values of die 2 meaning different things given die 1 is always 1 for this part to be executed
      else if (die2 == 4){System.out.println("Fever Five!");}
      else if (die2 == 5){System.out.println("Easy Six!");}
      else if (die2 == 6){System.out.println("Seven Out!");}
    
                       }//end die1 = 1 case 
    if (die1 == 2){  //Determines the case die 1 has a value of 2, repeats the last if statement but with different combinations meaning different things, given die 1 has a value of 2 instead of 1 like before
      if (die2 == 1){System.out.println("Ace Deuce!");}
      else if (die2 == 2){System.out.println("Hard Four!");}
      else if (die2 == 3){System.out.println("Fever Five!");}
      else if (die2 == 4){System.out.println("Easy Six!");}
      else if (die2 == 5){System.out.println("Seven Out!");}
      else if (die2 == 6){System.out.println("Easy Eight!");}
    
                       }//end die1 = 2 case 
    if (die1 == 3){//Determines what happens if die 1 has a value of 3
      if (die2 == 1){System.out.println("Easy Four!");}
      else if (die2 == 2){System.out.println("Fever Five!");}
      else if (die2 == 3){System.out.println("Hard Six!");}
      else if (die2 == 4){System.out.println("Seven Out!");}
      else if (die2 == 5){System.out.println("Easy Eight!");}
      else if (die2 == 6){System.out.println("Nine!");}
      
    } // Ends die1 = 3 case
    
    if (die1 == 4){
      if (die2 == 1){System.out.println("Fever Five!");}
      else if (die2 == 2){System.out.println("Easy Six!");}
      else if (die2 == 3){System.out.println("Seven Out!");}
      else if (die2 == 4){System.out.println("Hard Eight!");}
      else if (die2 == 5){System.out.println("Nine!");}
      else if (die2 == 6){System.out.println("Easy Ten!");}
      
    }
    
    if (die1 == 5){
      if (die2 == 1){System.out.println("Easy Six!");}
      else if (die2 == 2){System.out.println("Seven Out!");}
      else if (die2 == 3){System.out.println("Easy Eight!");}
      else if (die2 == 4){System.out.println("Nine!");}
      else if (die2 == 5){System.out.println("Hard Ten!");}
      else if (die2 == 6){System.out.println("Yo-leven!");}
      
    }
    
    if (die1 == 6){
      if (die2 == 1){System.out.println("Seven Out!");}
      else if (die2 == 2){System.out.println("Easy Eight!");}
      else if (die2 == 3){System.out.println("Nine!");}
      else if (die2 == 4){System.out.println("Easy Ten!");}
      else if (die2 == 5){System.out.println("Yo-leven!");}
      else if (die2 == 6){System.out.println("Boxcars!");}
     
    }
   
    System.out.println(""); //spacing out for formatting
    System.out.println("");
    
    
    
    
  }//end method
  
} //end class